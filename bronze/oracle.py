def oracle(circuit,qreg):
	circuit.cz(qreg[0],qreg[1])

def oracle8(circuit,quantum_reg):
	circuit.x(quantum_reg[2])
	circuit.ccx(quantum_reg[2],quantum_reg[1],quantum_reg[3])
	circuit.cz(quantum_reg[3],quantum_reg[0])
	circuit.ccx(quantum_reg[2],quantum_reg[1],quantum_reg[3])
	circuit.x(quantum_reg[2])
	