<h1><a href="https://www.qturkey.org">QTurkey</a></h1>
<h2>Contributing</h2>
Anyone who wants to contribute the development of the project is welcomed. 
<br>
There are different ways that you can contribute; by directly developing the teaching material, or by reading the developed material and giving feedback.
<br>
To directly develop the material, you can fork the repo, and make changes to your own fork, and then open a pull request for the development branch of this repository. DO NOT open a pull request for the master branch.
<br>
After opening a pull request, we will wait for some time for people to see your changes, and give enough feedback. If both maintainers and the community are convinced, then we will merge your changes to the repository.
<h2>Maintainers</h2>
Özlem Salehi: ozlem.salehi [at] boun [dot] edu [dot] tr
<br>
Onurcan Bektaş: onurcan.bektas [at] metu [dot] edu [dot] tr
